<?php
	 session_start();
	 $nom = $_SESSION['USER']['nom'];
	$prenom = $_SESSION['USER']['prenom'];
	$email = $_SESSION['USER']['email'];
	$pswd = $_SESSION['USER']['pwd'];
	$image = $_SESSION['USER']['photo'];
?>

<!DOCTYPE html>
<html>
<head>
	<title>Profil</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/formulaire.css">
	<link rel="stylesheet" type="text/css" href="../css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">

	<style>
				@media screen and (max-width: 768px){
			.B2{
				display: none;
			}

			.slogan{
				display:none;
			}

		}

		@media only screen and (max-width: 760px){
			.logo{
				margin-right:260px;
			}

			.profil{
				margin-top:-120px;
			}

			.B2{
				display:none;
			}

			.slogan{
				display:none;
			}

			.B1{
				margin-right:10px;
				padding:15px;
			}

			.blocf{
				font-size:15px;
			}

			.bloc1-footer{
				font-size:10px;
			}

			.bloc2-footer{
				font-size:10px;
			}

			.bloc3-footer{
				font-size:10px;
			}

			.bloc-footer{
				font-size:15px;
			}

			.bloc5{
				font-size:10px;
				width:50%;
			}

			.bf{
				font-size:10px;
				width:70%;
			}

			.row2{
				font-size:10px;
			}

			.Z1{
				margin-left:95px;
			}

			
		}
	</style>

</head>
<body>
	<div class="container-fluid">
		<div class="row entete">
			<div class="nav navbar-nav col-md-4">
				<div class="col-md-3 nav navbar-nav">
					<img src="../photo/inc2.png" class="img-circle pull pull-right logo">
				</div>

				<div class="col-md-9 slogan">
					<p>Giving Life Support To Customers <br>
						Votre Service Client...</p>
				</div>
			</div>


			<div class="nav navbar-nav col-md-8 pull pull-right menu">
				<ul class="nav navbar-nav pull pull-right">
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle profil"  id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    <?php echo "<img class='img-circle pull pull-right' style='width:60px; height:60; margin-right:20px' src='../images/".$_SESSION['USER']['photo']."'>" ?>
						    <p><span class="nom1"> <?php echo $_SESSION['USER']['nom']." ".$_SESSION['USER']['prenom'] ?></span></p>
						</a>

						<ul class="dropdown-menu pull pull-right">
							   <li class="divider"></li>
							   <li><a href="modification_profil.php">Mon profil</a></li>
							   <li class="dropdown-divider"></li>
							   <li><a href="deconnexion.php">Deconnexion</a></li>
							  <li class="divider"></li>
						</ul>
					</li>
				</ul>
			</div>
				
		</div>

				<!-- *************************************FIN DE L'ENTETE****************************** -->
				<!-- *************************************DEBUT DU BODY****************************** -->

		

				<!-- *************************************FIN DU BODY****************************** -->
				<!-- *************************************DEBUT DU FOOTER****************************** -->

		<div class="row footer">
			<div class="col-md-12">
				<div class="col-md-3 bloc-footer">
					<div class="row">
						<div class="col-md-12 blocf">
							Pourquoi nous?
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 bloc1-footer">
							<ul style="list-style:none">
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Satisfaction</span></li>
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Sécurité</span></li>
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Fiabilité</span></li>
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Garantie</span></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-3 bloc-footer">
					<div class="row">
						<div class="col-md-12 blocf">
							Adresses
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 bloc2-footer">
							<ul class=" " style="list-style:none">
								<li><span class="glyphicon glyphicon-globe icon"></span>&nbsp Pays: &nbsp <span class="p-footer">Cameroun</span></li>

								<li><span class="glyphicon glyphicon-map-marker icon"></span>&nbsp Ville: &nbsp <span class="p-footer">Douala</span></li>

								<li><span class="glyphicon glyphicon-phone icon"></span>&nbsp Téléphone: &nbsp <span class="p-footer">(+237) 671 316 424</span></li>

								<li><span class="glyphicon glyphicon-envelope icon"></span>&nbsp Email:&nbsp <span class="p-footer">tech.inc.cm@gmail.com</span></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-3 bloc-footer">
					<div class="row">
						<div class="col-md-12 blocf">
							Suivez-Nous
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 bloc3-footer">
							<ul style="list-style:none">
								<li><span><i class="fa fa-facebook facebook"></i>&nbsp Facebook: <a href=""><span class="p-footer">Tech-Inc</span></a></li>

								<li><span><i class="fa fa-twitter twitter"></i></span>&nbsp Twitter: &nbsp <a href=""><span class="p-footer">TechInc7</span></a></li>

								<li><span><i class="fa fa-linkedin linkedin"></i></span>&nbsp LinkedIn: &nbsp <a href=""><span class="p-footer">Tech-Inc</span></a></li>

								<li><span><i class="fa fa-whatsapp whatsapp"></i></span></span>&nbsp Whatsapp:&nbsp <a href=""><span class="p-footer">(+237) 671 316 424</span></a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-3 bloc-footer">
					<div class="row">
						<div class="col-md-12">
							<label class="bloc4">Newsletter</label>
							<div class="input-group bloc5">
								<form>	
									<input class="form-control" type="text" name="text" placeholder="Type Your Email Yere" style="font-style:italic; font-family:arial narrow">
									<button class="btn pull pull-right bf">Subscribe</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-12">
				<ul class="row2" style="list-style:none; text-align:center;">
					<li>Copyright © 2021 Tech-Inc</li>
					<li>Powered by Tech-Inc</li>
				</ul>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="../javascript/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../javascript/javascript.js"></script>
</body>
</html>