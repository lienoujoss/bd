<?php 
   session_start();
?>

<?php

if ( isset($_POST) ) {  // reception donnees et particulierement Email
  $bdd = new PDO('mysql:host=localhost;dbname=bd','root','', array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION)); // connect database
  $nom = $_POST['nom'];
  $prenom = $_POST['prenom'];
  $mail = $_POST['email'];
  $dataInsert = $bdd->prepare('UPDATE utilisateur SET nom = ?, prenom = ? WHERE email = ?');
  $dataInsert->execute( array($nom,$prenom,$mail));
  if (!empty($_POST['pwd'])) {
    $pswd = $_POST['pwd'];
    $dataInsert = $bdd->prepare('UPDATE utilisateur SET pwd = ? WHERE email = ?');
    $dataInsert->execute( array($pswd,$mail));
  }

}else{
  $_SESSION['message_error']=" Erreur Reception Des donnees !";
  header('location: modification_profil.php');  // redirection vers profil car  donnees pas recu
  }
      
if (isset($_FILES['photo']) AND $_FILES['photo']['error'] == 0) // verif et enregistrement image
{ 
 // $bdd = new PDO('mysql:host=localhost;dbname=users','root','', array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION)); // connect database
 // $mail = $_POST['email'];
 // $response = $bdd->query('SELECT photo FROM utilisateur WHERE email = $mail');
 if (($_FILES['photo']['size'] <= 3000000) AND ($_SESSION['USER']['photo'] != $_FILES['photo']['name']))
 {
    $infosfichier = pathinfo($_FILES['photo']['name']);
    $extension_upload = $infosfichier['extension'];
    $extensions_autorisees = array('jpg', 'jpeg', 'png','JPEG','PNG','JPG');
    if (in_array($extension_upload, $extensions_autorisees))
    {
       move_uploaded_file($_FILES['photo']['tmp_name'], '../Images/' . basename($_FILES['photo']['name']));
       $statut_img="ok";
    }else{
       echo 'Extention de  l\'Image non-autorisee';
       $statut_img="non";
    }
 }else{
    echo ' votre Image est trop Volumineuse';
    $statut_img="non";
 }
}else{
  $_SESSION['message_error']=" Erreur lors de la transmission de l'image !";
  header('location: modification_profil.php');  // redirection vers profil car  donnees pas recu
  }

if ($statut_img =="ok") {  // insertion dans database
   $bdd = new PDO('mysql:host=localhost;dbname=bd','root','', array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION)); // connect database
   $pnom= $_FILES['photo']['name'];
   $mail = $_POST['email'];
   $dataInsert=$bdd->prepare('UPDATE utilisateur SET photo = ? WHERE email = ?');
   $dataInsert->execute( array($pnom,$mail));
   $response = $bdd->query('SELECT * FROM utilisateur');
      $i = 0;
      $allUser = array();
      while ( $donnees = $response->fetch() ) {
         if ($donnees['email'] == $mail) {
           $allUser[$i] = $donnees;
           break;
         }
         $i++;
    }
    $_SESSION['USER'] = $allUser[$i];
   header('location: modification_profil.php');  // recuperation parametre user et redirect vers account
} else{
     $_SESSION['message_error']=" Veillez respecter le format d'image autorise !";
     header('location: modification_profil.php');  // redirection vers profil car image incorrecte
  }
     
  // print_r($_POST);
  // print_r($_FILES);
?>

