-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 02 juil. 2021 à 16:50
-- Version du serveur :  10.1.26-MariaDB
-- Version de PHP :  7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bd`
--

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pwd` varchar(255) NOT NULL,
  `niveau` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `nom`, `prenom`, `photo`, `email`, `pwd`, `niveau`) VALUES
(1, 'Super', 'admin', 'team2.jpg', 'superadmin@gmail.com', 'superadmin', 5),
(3, 'lienou', 'zedong', 'simplon.jpg', 'lienou@gmail.com', 'azerty', 1),
(4, 'derti', 'derti', 'simplon.jpg', 'derti@gmail.com', 'kpkpkkpkpk', 1),
(5, 'uoi', 'youi', 'inc4.jpeg', 'youi@gmail.com', 'klkllklklk', 1),
(6, 'william', 'william', 'inc.jpg', 'william@gmail.com', '1234', 1),
(7, 'fhjdshfdjsf', 'fdsdjfkjsdkfds', 'WhatsApp Image 2021-06-04 at 10.55.38.jpeg', 'lienoujoss@gmail.com', 'jfksdjfksdfsd', 1),
(8, 'inch1', 'class', 'simplon.jpg', 'inch@gmail.com', 'azerty', 1),
(9, 'toto', 'toto', 'simplon.jpg', 'toto@gmail.com', 'wxcvb', 1),
(10, 'tost', 'test', '580b57fcd9996e24bc43c543.png', 'test@gmail.com', '12345', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
